#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <assert.h>
#include <conio.h>
#include <string>     
#include <vector>
#include <fstream>
#include <Windows.h>
#include <map>
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#include "boost\filesystem.hpp"


extern std::string file_name;

#include <io.h>

FILE* read_file();
bool FileExists(const char *fname);
std::string create_dir_on_patch(std::string file_name);

void parse_texture(FILE* sfile, char* data, int pos_data, char* file_name);
void parse_textureUI(FILE* sfile, char* data, int pos_data, char* file_name);