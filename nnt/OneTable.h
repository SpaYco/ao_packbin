#pragma once
#include "Table.h"
#include "XdbOneTable.h"

class OneTable : public Table
{
public:
	OneTable() : Table() {};
	~OneTable();
	void init();
	void ParseTable();
	void ParseData(int sposition, int len, int eposition);
	static OneTable* instance()
	{
		static OneTable instance;
		return &instance;
	}

private:
	static OneTable * p_instance;
};

#define sOneTable OneTable::instance()