#include "Parsers.h"

void Parser::AnimationEvent() {
	int len_one = 24;

	this->SkipInt();
	int len = this->readIntBody();
	this->Skip(8); // 4 + 4
	this->start("animationEvents");
	for (int cx = 0; cx < len / len_one;cx++)
	{
		this->start("Item");

		this->SkipIntData();
		this->SkipData(12); // name
		int id = this->readIntData();
		if (id == 1)
			this->PrintNameValue("name", "Sound");

		if(id==0)
			this->PrintNameValue("name", "Action");

		if(id==2)
			this->PrintNameValue("name", "ActionPeriod");
			
		//TEST
		if (id > 2)
		{
			printf("ERROR UNK: %i name on id AnimationEvent file %s \n", id, this->file_name.c_str());
			_getch();
		}

		this->PrintNameValue("id", id);
		this->GetFloat5Data("event");

		this->finish("Item");
	}
	this->finish("animationEvents");

}