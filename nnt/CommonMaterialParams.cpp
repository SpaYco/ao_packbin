#include "Parsers.h"

int Parse_CommonMaterialParams(FILE* file, int position, char* type, int body_len) {

	Parser* CommonMaterialParams = new Parser(file, position, "CommonMaterialParams", body_len);
	if (CommonMaterialParams->ChekSubDataBlockXDB()) {
		CommonMaterialParams->GetBoolMini("vertexBakedLight");
		CommonMaterialParams->GetBoolMini("useMaskColor", 2);
		CommonMaterialParams->GetInt("specularColorModifier");
		CommonMaterialParams->GetBoolMini("selfillum", 3);
		CommonMaterialParams->GetLinkTexture("envSpecularTexture");
		CommonMaterialParams->GetLinkTexture("envReflectionTexture");
		CommonMaterialParams->GetLinkTexture("effectMaskTexture");
		CommonMaterialParams->GetInt("contourColorModifier");
	}
	return CommonMaterialParams->getPosiion();
}

