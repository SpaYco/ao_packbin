#include "Parsers.h"

int Parse_modelElements(FILE* file, int position, char* type, int body_len) {

	Parser* modelElements = new Parser(file, position, "modelElements", body_len);
	int modelElementsCount = body_len / 128;

	for (int cx = 0; cx < modelElementsCount;cx++)
	{
		int asd= modelElements->getPosiion();
		modelElements->startItem("Item");

		modelElements->GetFloat("virtualOffset");
		modelElements->GetInt("vertexDeclarationID");
		modelElements->GetInt("vertexBufferOffset");
		modelElements->GetInt("skinIndex");
		modelElements->GetStr("name");
		modelElements->GetStr("materialName");
		modelElements->startItem("material");
		modelElements->GetBool("visible");
		modelElements->GetFloat("vTranslateSpeed");
		modelElements->GetBool("useFog");
		modelElements->GetFloat("uTranslateSpeed");
		modelElements->GetBool("transparent");
		modelElements->GetLinkTexture("transparencyTexture");
		modelElements->GetBoolMini("scrollRGB");
		modelElements->GetBoolMini("scrollAlpha", 2);
		int as = modelElements->readIntBody();
		modelElements->start("params", "CommonMaterialParams");
		modelElements->setDataPosiion(Parse_CommonMaterialParams(modelElements->file, modelElements->getDataPosiion(), "CommonMaterialParams", 0));
		modelElements->finish("params");
		modelElements->GetLinkTexture("diffuseTexture"); //not valid	
		modelElements->Skip(4); //need 
		modelElements->finish("material");

		modelElements->start("lods");
		modelElements->setDataPosiion(Parse_GeometryFragment(modelElements->file, modelElements->getDataPosiion(), "lods", modelElements->getSubTableLen()));
		modelElements->finish("lods");
		modelElements->Skip(16);
		modelElements->finish("Item");
	}

	return modelElements->getDataPosiion();
}