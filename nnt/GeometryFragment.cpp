#include "Parsers.h"

int Parse_GeometryFragment(FILE* file, int position, char* type, int body_len) {

	Parser* GeometryFragment = new Parser(file, position, "MaterialInstance", body_len);
	int GeometryFragmentCount = body_len / 20;
	
	for (int cx = 0; cx < GeometryFragmentCount;cx++)
	{
		GeometryFragment->GetInt("vertexBufferEnd");
		GeometryFragment->GetInt("vertexBufferBegin");
		GeometryFragment->GetInt("indexBufferEnd");
		GeometryFragment->GetInt("indexBufferBegin");GeometryFragment->Skip(4);
	}
	return GeometryFragment->getDataPosiion();
}
