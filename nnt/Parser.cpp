#include "Parsers.h"
#include "Parser_structs.h"
#include "boost\filesystem.hpp"
#include <boost/filesystem/fstream.hpp>
#include <iostream>

Parser::Parser(FILE* sfile, char* file_name, char* type, int body_len)
{

}

int Parser::getPosiion() {
	return ftell(this->sfile);
}

int Parser::getDataPosiion() {
	return ftell(this->data_file);
}

int Parser::setDataPosiion(int pos) {
	return fseek(this->data_file, pos, SEEK_SET);
}

std::string Parser::getFileNameOnPos(int position)
{
	return (std::string)sXdbOneTable->files.find(position)->second.name;
}

void Parser::GetLinkTexture(std::string name) {
	int len = this->readIntBody();
	int len2 = this->readIntBody();
	if (sXdbOneTable->files.find(this->getDataPosiion()) != sXdbOneTable->files.end())
	{
		this->PrintHref(name, this->getFileNameOnPos(this->getDataPosiion()) + "#xpointer(/Texture)");
		this->SkipData(parse_texture(read_file(), this->getDataPosiion(), this->getFileNameOnPos(this->getDataPosiion())));
	}else
	this->PrintHref(name, "");
}

void Parser::PrintHref(std::string name, std::string value)
{
	#ifdef _DEBUG
		printf("<%s href=\"%s\" />\n", name.c_str(), value.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave)
			fprintf(file, "<%s href=\"%s\" />\n", name.c_str(), value.c_str());
	#endif
};

Parser::Parser(FILE* file, int position, char* type, int body_len)
{
	this->fsave = true;
	this->file = file;
	this->sfile = read_file();
	fseek(this->sfile, position, SEEK_SET);

	this->BodyLen = body_len;
	this->data_file_position = ftell(sfile) + body_len;
	this->data_file = read_file();
	fseek(data_file, data_file_position, SEEK_SET);
}

Parser::Parser(FILE* sfile, std::string file_name, char* type, int body_len)
{
	this->sfile = sfile;
	this->type = type;
	this->file_name = file_name;
	this->data_file_position = ftell(sfile)+body_len;
	this->BodyLen = body_len;

	this->data_file = read_file();
	fseek(data_file, data_file_position, SEEK_SET);

	#ifndef _aDEBUG
	std::string name;
	name.append(file_name);
	std::string result = name.substr(name.find_last_of("\\") + 1, std::string::npos);
	std::string filename = name.substr(name.find_last_of("/") + 1, std::string::npos);
	name.resize(name.length() - filename.length());
	std::string filepath = "Out/" + name;
	std::string file_names = filepath + filename;
	boost::filesystem::create_directories(filepath.c_str());
	//printf("dasdas");_getch();
	if (!FileExists(file_names.c_str())) {
		this->file = fopen(file_names.c_str(), "w+");
		this->fsave = true;
	}
		printf("File: %s\n", file_name.c_str());
	#endif

	#ifdef _DEBUG
	printf("<?xml version=\"1.0\" encoding=\"UTF - 8\" ?> \n<%s>\n", type);
	#endif

	#ifndef _aDEBUG
	if (this->fsave)
		fprintf(file, "<?xml version=\"1.0\" encoding=\"UTF - 8\" ?> \n<%s>\n", type);
	#endif

	
}

void Parser::start(std::string name) {

	#ifdef _DEBUG
		printf("<%s>\n", name.c_str());
	#endif	

	#ifndef _aDEBUG
		if (this->fsave)
			fprintf(file, "<%s>\n", name.c_str());
	#endif

}

void Parser::start(std::string name, std::string param) {

	#ifdef _DEBUG
		printf("<%s type=\"%s\">\n", name.c_str(), param.c_str());
	#endif	

	#ifndef _aDEBUG
		if (this->fsave)
			fprintf(file, "<%s type=\"%s\">\n", name.c_str(), param.c_str());
	#endif
}

void Parser::startItem(std::string name) {

this->Skip(4);

	#ifdef _DEBUG
		printf("<%s>\n", name.c_str());
	#endif	

	#ifndef _aDEBUG
		if (this->fsave)
			fprintf(file, "<%s>\n", name.c_str());
	#endif

}

void Parser::finish(std::string name) {

	#ifdef _DEBUG
		printf("</%s>\n", name.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave)
			fprintf(file, "</%s>\n", name.c_str());
	#endif
		
}

void Parser::PrintNullhref(std::string name) {

	#ifdef _DEBUG
		printf("<%s href=\"\" />\n", name.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave)
			fprintf(file, "<%s href=\"\" />\n", name.c_str());
	#endif

}
void Parser::PrintNullTag(std::string name) {

	#ifdef _DEBUG
		printf("<%s />\n", name.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave)
			fprintf(file, "<%s />\n", name.c_str());
	#endif
}
void Parser::PrintNameValue(std::string name, std::string value)
{
	#ifdef _DEBUG
		printf("<%s>%s</%s> \n", name.c_str(), value.c_str(), name.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave)
			fprintf(file, "<%s>%s</%s> \n", name.c_str(), value.c_str(), name.c_str());
	#endif
}

void Parser::PrintNameValue(std::string name, int value)
{
	#ifdef _DEBUG
		printf("<%s>%i</%s> \n", name.c_str(), value, name.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave)
			fprintf(file, "<%s>%i</%s> \n", name.c_str(), value, name.c_str());
	#endif
}

//Printable function's
void Parser::GetInt(std::string name)
{
	int ss = this->readIntBody();
	if ((name == "sourceFileCRC") && (ss == 256))
		printf("213");

	#ifdef _DEBUG
		printf("<%s>%i</%s> \n", name.c_str(), ss, name.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave)
			fprintf(file, "<%s>%i</%s> \n", name.c_str(), ss, name.c_str());
	#endif
}

void Parser::GetFloat(std::string name)
{
	float value = this->readFloatBody();

	#ifdef _DEBUG
		printf("<%s>%.1f</%s> \n", name.c_str(), value, name.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave)
			fprintf(file, "<%s>%.1f</%s> \n", name.c_str(), value, name.c_str());
	#endif
}

void Parser::GetAABB(std::string name)
{
	AABB *aabb;
	aabb = new AABB[1];
	fread(aabb, sizeof(m_AABB), 1, sfile);

	#ifdef _DEBUG
		printf("<%s>\n", name.c_str());
		printf("<center x=\"%.8f\" y=\"%.8f\" z=\"%.8f\"/>\n", aabb->aabb_center_x, aabb->aabb_center_y, aabb->aabb_center_z);
		printf("<extents x=\"%.8f\" y=\"%.8f\" z=\"%.8f\"/>\n", aabb->aabb_extents_x, aabb->aabb_extents_y, aabb->aabb_extents_z);
		printf("</%s>\n", name.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave) {
			fprintf(file, "<%s>\n", name.c_str());
			fprintf(file, "<center x=\"%.8f\" y=\"%.8f\" z=\"%.8f\"/>\n", aabb->aabb_center_x, aabb->aabb_center_y, aabb->aabb_center_z);
			fprintf(file, "<extents x=\"%.8f\" y=\"%.8f\" z=\"%.8f\"/>\n", aabb->aabb_extents_x, aabb->aabb_extents_y, aabb->aabb_extents_z);
			fprintf(file, "</%s>\n", name.c_str());
		}
	#endif
}

int Parser::getSubTableLen()
{
	subTable *msubTable;
	msubTable = new subTable[1];
	fread(msubTable, sizeof(m_subtable), 1, sfile);
	return msubTable->len;
}

void Parser::GetQuat(std::string name)
{
	QUAT *quat;
	quat = new QUAT[1];
	fread(quat, sizeof(m_QUAT), 1, sfile);

	#ifdef _DEBUG
			printf("<%s x=\"%e\" y=\"%e\" z=\"%e\" w=\"%.1f\" />\n",name.c_str(), quat->x, quat->y, quat->z, quat->w);
	#endif

	#ifndef _aDEBUG
		if (this->fsave) {
			fprintf(file, "<%s x=\"%e\" y=\"%e\" z=\"%e\" w=\"%.1f\" />\n", name.c_str() , quat->x, quat->y, quat->z, quat->w);
		}
	#endif

}

void Parser::GetVEC(std::string name)
{
	VEC3 *vec;
	vec = new VEC3[1];
	fread(vec, sizeof(m_VEC3), 1, sfile);

	#ifdef _DEBUG
		printf("<%s x=\"%e\" y=\"%e\" z=\"%e\" />\n", name.c_str(), vec->x, vec->y, vec->z);
	#endif

	#ifndef _aDEBUG
		if (this->fsave) {
			fprintf(file, "<%s x=\"%e\" y=\"%e\" z=\"%e\"/>\n", name.c_str(), vec->x, vec->y, vec->z);
		}
	#endif

}

void Parser::GetBool(std::string name) {
	int value = this->readIntBody();

	#ifdef _DEBUG
		if (value == 0)
			printf("<%s>false</%s>\n", name.c_str(), name.c_str());
		else
			printf("<%s>true</%s>\n", name.c_str(), name.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave) {
			if (value == 0)
				fprintf(file, "<%s>false</%s>\n", name.c_str(), name.c_str());
			else
				fprintf(file, "<%s>true</%s>\n", name.c_str(), name.c_str());
		}
	#endif

}

void Parser::GetBoolMini(std::string name) {
	bool value = false;
	fread(&value, sizeof(bool), 1, sfile);

	#ifdef _DEBUG
		if (value == false)
			printf("<%s>false</%s>\n", name.c_str(), name.c_str());
		else
			printf("<%s>true</%s>\n", name.c_str(), name.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave) {
			if (value == 0)
				fprintf(file, "<%s>false</%s>\n", name.c_str(), name.c_str());
			else
				fprintf(file, "<%s>true</%s>\n", name.c_str(), name.c_str());
		}
	#endif
}

void Parser::GetBoolMini(std::string name, int skip)
{
	this->GetBoolMini(name);
	this->Skip(skip);
}


void Parser::GetBlob(std::string name) {

	aBLOB *blob;
	blob = new aBLOB[1];
	fread(blob, sizeof(m_BLOB), 1, sfile);

	#ifdef _DEBUG
		printf("<%s>\n", name.c_str());
		printf("<localID>%i</localID>\n", blob->localID);
		printf("<size>%i</size>\n", blob->size);
		printf("</%s>\n", name.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave) {
			fprintf(file, "<%s>\n", name.c_str());
			fprintf(file, "<localID>%i</localID>\n", blob->localID);
			fprintf(file, "<size>%i</size>\n", blob->size);
			fprintf(file, "</%s>\n", name.c_str());
		}
	#endif

}

void Parser::GetTextureType()
{
	int value = this->readIntBody();

	#ifdef _DEBUG
		printf("<type>%s</type>\n", get_texture_type(value).c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave) {
			fprintf(file, "<type>%s</type>\n", get_texture_type(value).c_str());
		}
	#endif
}
//Read Function
int Parser::readIntBody()
{
	int value = 0;
	fread(&value, sizeof(int), 1, sfile);
	return  value;
}

float Parser::readFloatBody(){

	float value = 0;
	fread(&value, sizeof(float), 1, sfile);
	return  value;
}

//Printable Data Read Function
void Parser::GetFileName(std::string name) {
	std::string file_name_href;

	SkipInt(); //unk1
	int len =  this->readIntBody();

	SkipInt(); //unk2
	SkipInt(); //unk3
	SkipInt(); //unk4

	if (len > 0)
		file_name_href = GetString(len).c_str();
	else
		file_name_href = "";

	#ifdef _DEBUG
		printf("<%s href=\"%s\" />\n", name.c_str(), file_name_href.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave) {
			fprintf(file, "<%s href=\"%s\" />\n", name.c_str(), file_name_href.c_str());
		}
	#endif
}

void Parser::GetStr(std::string name) {
	sstring *msstring;
	msstring = new sstring[1];
	fread(msstring, sizeof(m_sstring), 1, sfile);
	std::string value = this->GetString(msstring->len).c_str();

	#ifdef _DEBUG
		printf("<%s>%s</%s> \n", name.c_str(), value.c_str(), name.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave) {
			fprintf(file, "<%s>%s</%s> \n", name.c_str(), value.c_str(), name.c_str());
		}
	#endif
}

int Parser::readIntData()
{
	int value = 0;
	fread(&value, sizeof(int), 1, data_file);
	return  value;
}

float Parser::readFloatData()
{
	float value = 0;
	fread(&value, sizeof(float), 1, data_file);
	return  value;
}

void Parser::GetIntData(std::string name)
{
	int value = this->readIntData();
	#ifdef _DEBUG
		printf("<%s>%i</%s> \n", name.c_str(), value, name.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave) {
			fprintf(file, "<%s>%i</%s> \n", name.c_str(), value, name.c_str());
		}
	#endif
}

void Parser::GetHideRule() {

	std::string value;
	switch (this->readIntBody())
	{
	case 0:
		value = "DISABLE";
		break;
	case 1:
		value = "ENABLE";
		break;
	default:
		value = "DEFAULT";
		break;
	}
	
	#ifdef _DEBUG
		printf("<hideRule>%s</hideRule> \n", value.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave) {
			fprintf(file, "<hideRule>%s</hideRule> \n", value.c_str());
		}
	#endif
}

void Parser::GetOrientationMode() {

	std::string value;
	switch (this->readIntBody())
	{
	case 0:
		value = "COMMON";
		break;
	case 1:
		value = "WORLD_Z";
		break;
	case 2:
		value = "Z_AXIS";
		break;
	case 3:
		value = "BILLBOARD";
		break;
	default:
		value = "COMMON";
		break;
	}

	#ifdef _DEBUG
		printf("<orientationMode>%s</orientationMode> \n", value.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave) {
			fprintf(file, "<orientationMode>%s</orientationMode> \n", value.c_str());
		}
	#endif

}

void Parser::getVertexComponentType() {

	std::string value;
	switch (this->readIntData())
	{
	case 2:
		value = "FLOAT2";
		break;
	case 3:
		value = "FLOAT3";
		break;
	case 4:
		value = "FLOAT4";
		break;
	case 5:
		value = "SHORT2";
		break;
	case 6:
		value = "SHORT4";
		break;
	case 7:
		value = "COLOR4";
		break;
	case 8:
		value = "UBYTE4";
		break;
	case 9:
		value = "USH0RT2";
		break;
	case 10:
		value = "USH0RT4";
		break;
	case 11:
		value = "HALF4";
		break;
	case 12:
		value = "UNUSED";
		break;
	default:
		value = "FLOAT1";
		break;
	}

	#ifdef _DEBUG
		printf("<type>%s</type> \n", value.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave) {
			fprintf(file, "<type>%s</type> \n", value.c_str());
		}
	#endif

}


void Parser::GetSortMode()
{
	std::string value;
	switch (this->readIntBody())
	{
	case 1: value = "FRONT_BACK"; break;
	case 2: value = "BACK_FRONT"; break;
	case 3: value = "OFFSETS";  break;
	default:
		value = "OFFSETS";
		break;
	}

	#ifdef _DEBUG
		printf("<sortMode>%s</sortMode> \n",  value.c_str() );
	#endif

	#ifndef _aDEBUG
		if (this->fsave) {
			fprintf(file, "<sortMode>%s</sortMode> \n", value.c_str());
		}
	#endif
}


void Parser::GetFloatData(std::string name)
{
	float value = this->readFloatData();

	#ifdef _DEBUG
		printf("<%s>%.0f</%s> \n", name.c_str(), value, name.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave) {
			fprintf(file, "<%s>%.0f</%s> \n", name.c_str(), value, name.c_str());
		}
	#endif

}

void Parser::GetFloat5Data(std::string name)
{

	float value = this->readFloatData();

	#ifdef _DEBUG
		printf("<%s>%.5f</%s> \n", name.c_str(), value, name.c_str());
	#endif

	#ifndef _aDEBUG
		if (this->fsave) {
			fprintf(file, "<%s>%.5f</%s> \n", name.c_str(), value, name.c_str());
		}
	#endif

}
//Read String
std::string Parser::GetString(int len) {
	if (len > 255)
		return "";

	char* c = new char[len]();
	fread(c, sizeof(char), (size_t)len, this->data_file);


	std::string value(c, len);
	delete(c);
	if ((len % 4)) {
		for (int i = len; i < 200; ++i)
			if (!(i % 4)) {
				fseek(this->data_file, ftell(this->data_file) + (i - len), SEEK_SET);
				return value;
			}
	}
	else {
		fseek(this->data_file, ftell(this->data_file) + 4, SEEK_SET);
		return value;
	}

}

//Skip
void Parser::SkipInt() {
	fseek(sfile, ftell(sfile) + 4, SEEK_SET);
}

void Parser::Skip(int bytes)
{
	fseek(sfile, ftell(sfile) + bytes, SEEK_SET);
}

void Parser::SkipIntData() {
	fseek(data_file, ftell(data_file) + 4, SEEK_SET);
}

void Parser::SkipData(int bytes)
{
	fseek(data_file, ftell(data_file) + bytes, SEEK_SET);
}

bool Parser::ChekSubDataBlockXDB() {
	sChekSubDataBlockXDB *nChekSubDataBlockXDB;
	nChekSubDataBlockXDB = new sChekSubDataBlockXDB[1];
	fread(nChekSubDataBlockXDB, sizeof(m_sChekSubDataBlockXDB), 1, sfile);
	if (nChekSubDataBlockXDB->unk1 == 0 && nChekSubDataBlockXDB->unk2 == 1 && nChekSubDataBlockXDB->unk3 == 0 && nChekSubDataBlockXDB->unk4 == 0 && nChekSubDataBlockXDB->unk5 == 0 && nChekSubDataBlockXDB->unk6 == 0)
		return true;
	else {
		this->Skip(-24);
		return false;
	}
	

}

int Parser::save() {

if (this->fsave) 
	fclose(file);

this->EndDataPosition = this->getDataPosiion();
this->Lenght = this->BodyLen + (this->EndDataPosition - this->data_file_position);
fclose(this->data_file);
return this->Lenght;

};
Parser::~Parser()
{
	fclose(data_file);
}